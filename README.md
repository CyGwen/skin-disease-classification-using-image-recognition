# SKIN DISEASE CLASSIFICATION USING IMAGE RECOGNITION

Urgently and decisively diagnosing skin diseases guarantees their manageability. Automating the diagnosis by designing and implementing a web app with a machine learning model that can assist the dermatologist in doing so.

## REQUIREMENTS

This web application requires the following:

 * [Python](https://www.python.org/downloads/)
 * [Flask]

## INSTALLATION

Implement the following:
1. Make sure that Python is downloaded in your PC. If yes proceed to step 3. If no, download the installer 
   from the given URL above, then execute it.

   Copy the path of the Python bin folder and set aside. 
   Add Python to the System Environment Variable by searcing environment in the start button.
   Click 'Edit the system environment variable'
   Click Environment Variable ...
   Under System Variables, find 'path' and click edit
   Click New then paste the copied path.
   Click Ok in the Edit environment variable window
   Click Ok in the Environment Variables window
   Click Ok in the System Properties window
   Restart your PC.

2. Open your terminal and navigate to the Project directory.
3. Enter the following commands:
   cd src
   pip install Flask
   flask run

4. Enter the given link to a browser, or press control and click the link.

## OTHER INFORMATION

Links:
 * [WEBAPP](https://sdrecognition.herokuapp.com/)
 * [DATASET](https://www.kaggle.com/shubhamgoel27/dermnet)

## MAINTAINERS

Current maintainers:

 * John Lloyd Balcita
 * Gwyneth M. Calica
 * Riel Victor Cezar
 * Alvin John L. Cutay
 * Ben Carlo M. de los Santos
 * Lywe Joen Dup-et
 * Dale Gaddi
 * Chris Alejandro Palabay
 * Reimer Augustine Pare
 * Franz Nico Testado 