aiohttp==3.7.4
argon2-cffi==20.1.0
asgiref==3.3.1
async-generator==1.10
async-timeout==3.0.1
attrs==20.3.0
backcall==0.2.0
beautifulsoup4==4.9.3
bleach==3.3.0
Brotli==1.0.9
cffi==1.14.5
chardet==3.0.4
click==7.1.2
colorama==0.4.4
DateTime==4.3
decorator==4.4.2
defusedxml==0.6.0
dpd-components==0.1.0
dpd-static-support==0.0.5
entrypoints==0.3
env==0.1.0
Flask==1.1.2
Flask-Compress==1.9.0
Flask-SQLAlchemy==2.5.1
future==0.18.2
greenlet==1.1.0
gunicorn==20.1.0
idna==3.1
ipykernel==5.5.0
ipython==7.21.0
ipython-genutils==0.2.0
ipywidgets==7.6.3
itsdangerous==1.1.0
jedi==0.18.0
Jinja2==2.11.3
joblib==1.0.1
jsonschema==3.2.0
MarkupSafe==1.1.1
mistune==0.8.4
multidict==5.1.0
nbclient==0.5.3
nbconvert==6.0.7
nbformat==5.1.2
nbserverproxy==0.8.8
nest-asyncio==1.5.1
notebook==6.2.0
numpy==1.20.3
packaging==20.9
pandas==1.2.4
pandocfilters==1.4.3
parso==0.8.1
php==1.2.1
pickleshare==0.7.5
plotly==4.14.3
prometheus-client==0.9.0
prompt-toolkit==3.0.16
pycparser==2.20
Pygments==2.8.0
pyparsing==2.4.7
pyrsistent==0.17.3
python-dateutil==2.8.1
pytz==2020.1
pyzmq==22.0.3
qtconsole==5.0.2
QtPy==1.9.0
retrying==1.3.3
scikit-learn==0.24.2
scipy==1.6.3
Send2Trash==1.5.0
six==1.15.0
soupsieve==2.2
SQLAlchemy==1.4.18
sqlparse==0.4.1
terminado==0.9.2
testpath==0.4.4
threadpoolctl==2.1.0
tornado==6.1
traitlets==5.0.5
typing-extensions==3.7.4.3
wcwidth==0.2.5
webencodings==0.5.1
Werkzeug==1.0.1
widgetsnbextension==3.5.1
yarl==1.6.3
zope.interface==5.1.0