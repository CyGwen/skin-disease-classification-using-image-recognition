from flask import Flask, request, redirect, url_for, render_template, make_response, Response
import os
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = 'static/uploads/'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route("/")
def index():
    return render_template('index.html')

@app.route("/skin-diseases")
def skin_diseases():
    return render_template('skin-diseases.html')

@app.route("/result")
def get_result():
    # response = make_response(render_template('result.html', filename='captured.png'))
    # response.headers.set('Cache-Control', 'no-cache, no-store')
    # response.headers.set('Pragma', 'no-cache')
    # return response
    return render_template('result.html', filename='captured.png')
   
@app.route("/capture", methods=['POST'])
def take_picture():
    file = request.files['file']
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        response = Response()
        response.headers['Cache-Control'] = 'no-cache, no-store'
        response.headers['Pragma'] = 'no-cache'
        return response

@app.route("/upload", methods=['POST'])
def upload_image():
    file = request.files['file']
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return render_template('result.html', filename=filename)

@app.route('/display/<filename>')
def display_image(filename):
    return redirect(url_for('static', filename='uploads/' + filename), code=301)

@app.route('/upload')
def go_to_upload():
    return render_template('index.html', redirect='upload')

@app.route('/disease/<num>')
def disease(num):
    return render_template('skin-diseases.html', redirect=num)

if __name__ ==  "__main__":
    app.run()